import os, shutil
from pathlib import Path
from cv2 import invert
from jinja2 import Environment, FileSystemLoader

output_dir = "./output"
template_dir = "./templates"
line_template = "line_v1.svg"
max_length = 10
stroke_width = max_length / 40
color_background = "black"
color_line = "white"

# function to clear a given directory
def clear_dir(dir_name):
  for filename in os.listdir(dir_name):
    file_path = os.path.join(dir_name, filename)
    try:
      if os.path.isfile(file_path) or os.path.islink(file_path):
        os.unlink(file_path)
      elif os.path.isdir(file_path):
        shutil.rmtree(file_path)
    except Exception as e:
        print('Failed to delete %s. Reason: %s' % (file_path, e))

Path(output_dir).mkdir(parents=True, exist_ok=True)

env = Environment(
  loader=FileSystemLoader(template_dir)
)

template = env.get_template(line_template)

output_folder = '{}/{}'.format(output_dir, line_template)
Path(output_folder).mkdir(parents=True, exist_ok=True)
clear_dir(output_folder)

size = max_length + stroke_width

center = size / 2
for i in range(0, max_length+1, 1):
  output_file = '{}/{}.svg'.format(output_folder, i)
  inverted = i % 2
  print(inverted)
  if (inverted == 0):
    x1 = y2 = center - i / 2
    x2 = y1 = center + i / 2
    render_variables = {
      "size": size,
      "stroke_width": stroke_width,
      "pos1": {
        "x": x1,
        "y": y1
      },
      "pos2": {
        "x": x2,
        "y": y2
      },
      "color": {
        "background": color_background,
        "line": color_line
      }
    }
  else:
    x1 = y1 = center - i / 2
    x2 = y2 = center + i / 2
    render_variables = {
      "size": size,
      "stroke_width": stroke_width,
      "pos1": {
        "x": x1,
        "y": y1
      },
      "pos2": {
        "x": x2,
        "y": y2
      },
      "color": {
        "background": color_background,
        "line": color_line
      }
    }
  template_render = template.render(render_variables)
  with open(output_file, 'w') as f:
    f.write(template_render)
