# The longest - Generative art

Programmatically create svg images with lines with a different length.

## Development

The POC is done with python.

To run the POC you need to have python3 installed.

To setup the environment run the following commands:

```shell
python3 -m venv envs/generative-art
source envs/generative-art/bin/activate
pip install -r requirements.txt
```

To create the outputs from a template adjust the variables on the top of the python file and the run the following:

```shell
python3 generate_lines.py
```

To leave the python virtual environment run:
```shell
deactivate
```
